<?php 
/*
 * PHP 5.4.3
 * Maurucio Mage
 * 2015- 
 * -GEOgram
 */

if (! empty ( $_GET ['location'] )) {
	$LOCATION = $_GET ['location'];
	$LOCATION = str_replace ( " ", "", $LOCATION );
	
	$maps_url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $LOCATION;
	
	// Return JSON
	$maps_json = file_get_contents ( $maps_url );
	$maps_array = json_decode ( $maps_json, true );
	
	$lat = $maps_array ['results'] [0] ['geometry'] ['location'] ['lat'];
	$lng = $maps_array ['results'] [0] ['geometry'] ['location'] ['lng'];
	
	$instagram_url = 'https://api.instagram.com/v1/media/search?lat=' . $lat . '&lng=' . $lng . '&client_id=c09db7a0790a4e269854afe5b2a79ee2';
	$instagram_json = file_get_contents ( $instagram_url );
	
	$instagram_array = json_decode ( $instagram_json, true );
}

?>


<html>
<head>
<title>geoGRAM [OLAPIC] - MAGE MAURICIO</title>
</head>

<body>
	<form action="">
		<input type="text" name="location" />
		<button type="submit">SUBMIT</button>
		<br />
		<center>
			<div align="center"
				style="height: 600px; overflow-y: auto; overflow-x: hidden;">
			<?php
			if (! empty ( $instagram_array )) {
				foreach ( $instagram_array ['data'] as $image ) {
					
					$USERNAME = $image ['user'] ['username'];
					$USERNAME = strtoupper ( $USERNAME );
					
					$CITY = $image ['location'] ['name'];
					$CITY = strtoupper ( $CITY );
					
					/* GET LAT and LONG from Instagram Array */
					$LATITUDE_INSTAGRAM = $image ['location'] ['latitude'];
					$LONGITUDE_INSTAGRAM = $image ['location'] ['longitude'];
					
					/* GET address_components */
					$maps_url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $LATITUDE_INSTAGRAM . ',' . $LONGITUDE_INSTAGRAM;
					
					// Return JSON
					$maps_json = file_get_contents ( $maps_url );
					$maps_array = json_decode ( $maps_json, true );
					
					$address = $maps_array ['results'] [0] ['address_components'] [1] ['long_name'];
					$locality = $maps_array ['results'] [0] ['address_components'] [2] ['long_name'];
					
					echo '<font face="Calibri MS" size="2">Username :</font> <font face="Calibri MS" size="2"><b>' . $USERNAME . '</b></font><br/>';
					echo '<div style="border-radius:10px"><img src="' . $image ['images'] ['low_resolution'] ['url'] . '"  alt="" /></div>';
					// echo '<div style="border-radius:10px"><video controls><source src="'.$image['videos']['low_bandwidth']['url'].'" type="video/mp4" /></video></div>';
					echo '<font face="Calibri MS" size="2">Place :</font> <font face="Calibri MS" size="2"><b>' . $CITY . '</b></font><br/>';
					echo '<font face="Calibri MS" size="2">Address :</font> <font face="Calibri MS" size="2"><b>' . $address . '</b></font><br/>';
					echo '<font face="Calibri MS" size="2">Locality :</font> <font face="Calibri MS" size="2"><b>' . $locality . '</b></font><br/>';
					echo '</br>___________________________________________________</br></br>';
				}
			}
			?>
			</div>
		</center>
	</form>
</body>

</html>